#!/bin/python3
import time
startTime = time.time()

import sys, hashlib, os, signal, subprocess, pathlib

class sh_return(str):
    args = None
    returncode = None
    stdout = ''
    stderr = ''
    def __init__(self, s):
        self.args = s.args
        self.returncode = s.returncode
        if s.stdout:
            self.stdout = str(s.stdout,encoding='utf-8').strip()
        if s.stderr:
            self.stderr = str(s.stderr,encoding='utf-8').strip()
    def __str__(self):
        return self.stdout
    def __bool__(self):
        return self.returncode == 0
def sh(cmd,do_capture_output=True):
    if type(cmd) == str:
        return sh_return(subprocess.run(cmd,shell=True,capture_output=do_capture_output))
    if type(cmd) == list:
        return sh_return(subprocess.run(cmd,shell=False,capture_output=do_capture_output))
    return False

def sigint(sig, frame):
    print("\nSIGINT: download canceled")
    sys.exit(1)
signal.signal(signal.SIGINT, sigint)

def fsync(path):
    fd = os.open(path,os.O_NONBLOCK)
    os.fsync(fd)
    os.close(fd)

def PrintElapsedTime(msg=''):
    s = time.time() - startTime
    elapsed = s
    h, s = divmod(s,3600)
    m, s = divmod(s,60)
    h = int(h)
    m = int(m)
    if h > 0:
        h = str(h) + ':'
    else:
        h = ''
    if m > 0:
        m = str(m).zfill(2) + ':'
    else:
        m = ''
    s = str(round(s,3))
    print(f'{msg} {h}{m}{s}')
    return elapsed

# sizeof_fmt shamelessly stolen from https://stackoverflow.com/questions/1094841/get-human-readable-version-of-file-size
def sizeof_fmt(num, suffix="B"):
    for unit in ["", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]:
        if abs(num) < 1024.0:
            return f"{num:3.1f}{unit}{suffix}"
        num /= 1024.0
    return f"{num:.1f}Yi{suffix}"

def fatal(s):
    print(f'FATAL: {s}')
    sys.exit(1)

if len(sys.argv) < 3:
    print(f'''\
  Usage: {sys.argv[0]} adb-path local-path [-f] [-v] [-ff] [-s] [-ss]
Example: {sys.argv[0]} /storage/emulated/0/DCIM /tmp/DCIM
-f       force a reload of the file list from the device, don't use the cache
-v       do a second verification pass right away
-ff      force everything, no caches, no skips
-s       print less output (may go faster for verification and cache hits)
-ss      print even less output (only print statistics at the end)
''')
    sys.exit(1)

print('Waiting for device...')
sh('adb wait-for-device') or fatal("No device connected")
PrintElapsedTime('Device found')

if sh('adb get-state').stdout != 'device':
    fatal(f"Device state is: '{devState}', should be 'device'")

devSerial = sh('adb get-serialno').stdout

BASE_DIR = sys.argv[1] + '/'
DEST_DIR = sys.argv[2] + '/'

h = hashlib.sha256()
h.update(bytes(devSerial,encoding='utf-8'))
h.update(bytes(BASE_DIR,encoding='utf-8'))
sessionHash = h.hexdigest()

cache_find = pathlib.Path(f'/tmp/adb-fast-download-cache-{sessionHash}')
cache_done = pathlib.Path(f'/tmp/adb-fast-download-completed-{sessionHash}')

mainLoops = 1
if '-v' in sys.argv:
    mainLoops = 2

if '-f' in sys.argv or '-ff' in sys.argv:
    if cache_find.exists():
        cache_find.unlink()

silentMode = False
if '-s' in sys.argv:
    silentMode = True

superSilentMode = False
if '-ss' in sys.argv:
    superSilentMode = True

CACHE_COMPLETED = {}
CACHE_VERIFIED = {}
if cache_done.exists():
    CACHE_COMPLETED = {entry: True for entry in cache_done.read_text().split('\x00')[:-1]}
flDev = 'Unknown'
CACHE_DATA = ''
if cache_find.exists():
    print('Loading Cache...')
    cache_find_mtime = int(cache_find.stat().st_mtime)
    now = int(time.time())
    diff = now - cache_find_mtime
    if diff > (30 * 60 * 60):
        print('Cache too old')
        cache_find.unlink()
    else:
        CACHE_DATA = cache_find.read_text()
        flDev = f"No. File list loaded from cache: '{cache_find}' try adding -f to force reload from phone"
        PrintElapsedTime('Cache loaded')
if not cache_find.exists():
    print('Loading file list...')
    CACHE_DATA = sh(f'''\
            adb shell "find '{BASE_DIR}' -type f -printf '%s %T@ %P\\0'"\
            ''').stdout
    cache_find.write_text(CACHE_DATA)
    fsync(cache_find)
    PrintElapsedTime('File list loaded')
    flDev = "Yes. File list loaded from device, cache file will be valid for 30 hours, pass -f to force reload from phone"

forceAll = False
if '-ff' in sys.argv:
    CACHE_COMPLETED = {}
    CACHE_VERIFIED = {}
    forceAll = True

for x in range(mainLoops):
    filesDownloaded = 0
    filesScanned = 0
    filesChecked = 0
    filesCacheHit = 0
    totalDownloaded = 0
    notDownloaded = 0
    cache_done_file = cache_done.open(mode='a')
    for FILE_DATA in CACHE_DATA.split('\x00')[:-1]:
        FILE_SIZE, FILE_MTIME, FILE = FILE_DATA.split(' ',2)
        FILE_SIZE = int(FILE_SIZE)
        FILE_MTIME = int(float(FILE_MTIME))
        filesScanned += 1
        if CACHE_VERIFIED.get(FILE) or CACHE_COMPLETED.get(FILE):
            filesCacheHit += 1
            if not silentMode and not superSilentMode:
                print(f"SKIP: {FILE} (CACHE HIT)")
            continue
        SRC_FILE = BASE_DIR+FILE
        DEST_FILE = pathlib.Path(DEST_DIR+FILE)
        if not DEST_FILE.parent.exists():
            os.makedirs(DEST_FILE.parent, exist_ok=True)
        if not forceAll and DEST_FILE.exists():
            filesChecked += 1
            local_stat_data = DEST_FILE.stat()
            LOCAL_SIZE = int(local_stat_data.st_size)
            LOCAL_MTIME = int(local_stat_data.st_mtime)
            if (LOCAL_SIZE == FILE_SIZE) and (LOCAL_MTIME == FILE_MTIME):
                if not silentMode and not superSilentMode:
                    print(f"SKIP: '{FILE}' (SIZE+MTIME MATCH)")
                cache_done_file.write(FILE + '\x00')
                cache_done_file.flush()
                CACHE_VERIFIED[FILE] = True
                continue
        if not superSilentMode:
            print(f"DOWNLOAD: '{SRC_FILE}'")
        CACHE_VERIFIED[FILE] = False
        if not sh(['adb','pull','-a',SRC_FILE,DEST_FILE],False):
            if not sh('adb get-state').stdout == 'device':
                fatal('adb error')
            else:
                print(f"ERROR: Couldn't download '{SRC_FILE}'")
                notDownloaded += 1
        else:
            fsync(DEST_FILE)
        cache_done_file.write(FILE + '\x00')
        cache_done_file.flush()
        if not superSilentMode:
            print(f"DONE: '{DEST_FILE}'")
        filesDownloaded += 1
        totalDownloaded += FILE_SIZE
    totalDownloadedStr = sizeof_fmt(totalDownloaded)
    print(f"Files Loaded from Device: {flDev}")
    print(f"         Files Processed: {filesScanned}")
    print(f"              Cache Hits: {filesCacheHit}")
    print(f"        Files Downloaded: {filesDownloaded} ({totalDownloadedStr})")
    print(f"Files Unable to Download: {notDownloaded}")
    print(f"          Files Verified: {filesChecked}")
    s = PrintElapsedTime('Sync complete')
    if totalDownloaded > 0:
        bps = totalDownloaded // s
        bps_str = sizeof_fmt(bps)
        print(f" Effective Download Rate: {bps_str}/s")
    cache_done_file.close()
    cache_done.unlink()
    forceAll = False
PrintElapsedTime('Sync Script complete')
