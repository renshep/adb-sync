#!/bin/xonsh
#    Copyright (C) 2022  Naomi Steele - See LICENCE for more details
# imports and aliases
import sys
# classes and functions
class LocalPath():
    # shared
    def _po_upd(self):
        self.po = pf'{self.path}'
    def __init__(self,path):
        self.path = str(pf'{path}'.absolute())
        self._po_upd()
    def __str__(self):
        return self.path
    def endswith(self,val):
        return self.path.endswith(val)
    # needs to be overwritten in AdbPath
    is_adb = False
    is_local = True
    def __repr__(self):
        return f"LocalPath('{self.path}')"
    def mtime(self):
        return int(self.po.stat().st_mtime)
    def size(self):
        return int(self.po.stat().st_size)
    def parent(self):
        return LocalPath(str(self.po.absolute().parent))
    def append(self,data):
        return LocalPath(str(self) + str(data))
    def exists(self):
        self._po_upd()
        return self.po.exists()
    def is_dir(self):
        return self.po.is_dir()
    def is_file(self):
        return self.po.is_file()
    def mkdirs(self,path):
        return !(mkdir -p @(path))
    def iterdir(self):
        if not self.is_dir():
            return
        cur_dir = str(self)
        if not cur_dir.endswith('/'):
            cur_dir = cur_dir + '/'
        for item in self.po.iterdir():
            yield LocalPath(cur_dir + item)
        
class AdbPath(LocalPath):
    # overrides
    is_adb = True
    is_local = False
    def __repr__(self):
        return f"AdbPath('{self.path}')"
    def mtime(self):
        return int($(adb shell f"stat -c %Y '{self.path}'").strip())
    def size(self):
        return int($(adb shell f"stat -c %s '{self.path}'").strip())
    def parent(self):
        return AdbPath(str(self.po.absolute().parent))
    def append(self,data):
        return AdbPath(str(self) + str(data))
    def exists(self):
        return !(adb shell f"test -e '{self.path}'")
    def is_dir(self):
        return !(adb shell f"test -d '{self.path}'")
    def is_file(self):
        return !(adb shell f"test -f '{self.path}'")
    def mkdirs(self,path):
        return !(adb shell f"mkdir -p '{path}'")
    def iterdir(self):
        if not self.is_dir():
            return
        files = $(adb shell f"find '{self.path}' -maxdepth 1 -print0")
        files = files.split('\x00')
        for file in files[1:-1]:
            yield AdbPath(file)

def ParsePath(path,adb):
    if path.lower().startswith('adb:'):
        return AdbPath(path[4:])
    return LocalPath(path)

def do_copy(SRC_FILE,DEST_PATH):
    if not SRC_FILE.is_file():
        return False
    if not DEST_PATH.is_dir():
        return False
    echo f'COPY: {SRC_FILE} to {DEST_PATH}'
    if SRC_FILE.is_adb and DEST_PATH.is_adb:
        # adb -> adb, 'shell cp'
       return !(adb shell f"cp -f '{SRC_FILE}' 'DEST_PATH'")
    if SRC_FILE.is_adb and DEST_PATH.is_local:
        # adb -> local, 'pull'
        return !(adb pull -a @(SRC_FILE) @(DEST_PATH))
    if SRC_FILE.is_local and DEST_PATH.is_adb:
        # local -> adb, 'push'
        return !(adb push @(SRC_FILE) @(DEST_PATH))
    if SRC_FILE.is_local and DEST_PATH.is_local:
        # local -> local, 'cp'
        return !(cp -f @(SRC_FILE) @(DEST_PATH))

# parse params, print usage
if len($ARGS) < 3:
    print(f'''\
Usage: {$ARG0} SRC DEST [ADB...]
SRC probably *should* be directory, and DEST *must* be a directory
SRC and DEST can start with 'adb:' to indicate the remote filesystem
ADB is optional and can be used to change the 'adb' command to use
    (defaults to 'adb')

Example Usage:

# sync /storage/emulated/0/DCIM to the current directory
{$ARG0} adb:/storage/emulated/0/DCIM ./
''')
    sys.exit(1)

SRC_ARG = $ARG1
DEST_ARG = $ARG2
ADB = $ARGS[3:]
if len(ADB) == 0:
    ADB = ['adb']

# wait for adb device connection, if needed
src_needs_adb = SRC_ARG.lower().startswith('adb:')
dest_needs_adb = DEST_ARG.lower().startswith('adb:')
if src_needs_adb or dest_needs_adb:
    echo Waiting for device...
    if !(@(ADB) wait-for-device):
        echo Device connected: $(@(ADB) get-serialno)
    else:
        echo 'ERROR: no adb device found!'
        sys.exit(1)

# parse SRC
SRC = ParsePath(SRC_ARG,ADB)

# parse DEST
if DEST_ARG.endswith('/') == False:
    DEST_ARG = DEST_ARG + '/'
SRC_LAST = SRC_ARG.split('/')[-1]
if not SRC.is_file():
    DEST_ARG = DEST_ARG + SRC_LAST
if DEST_ARG.endswith('/') == False:
    DEST_ARG = DEST_ARG + '/'
DEST = ParsePath(DEST_ARG,ADB)

if not SRC.exists():
    echo f'ERROR: {SRC} does not exist'
    sys.exit(1)

if not DEST.exists():
    echo f'INFO: {DEST} does not exist, creating...'
    if DEST.mkdirs(DEST):
        echo f'INFO: {DEST} created.'

# so by now SRC and DEST exists
# make sure DEST is a directory
if not DEST.is_dir():
    echo f'ERROR: {DEST} exists and is not a directory, aborting...'
    sys.exit(1)

# now check if SRC is a dir
if not SRC.is_dir():
    if not SRC.is_file():
        echo 'ERROR: {SRC} is not a file or a directory, aborting...'
        sys.exit(1)
    else:
        # SRC is a file, just copy the one file
        sys.exit(0 if do_copy(SRC,DEST) else 1)

echo f"INFO: syncing '{SRC}' to '{DEST}'"
# main sync loop, SRC is a dir, DEST is a dir that exists

totalBytes = 0
totalBytesCopied = 0
filesSkipped = 0
# sizeof_fmt shamelessly stolen from https://stackoverflow.com/questions/1094841/get-human-readable-version-of-file-size
def sizeof_fmt(num, suffix="B"):
    for unit in ["", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]:
        if abs(num) < 1024.0:
            return f"{num:3.1f}{unit}{suffix}"
        num /= 1024.0
    return f"{num:.1f}Yi{suffix}"
def sync_copy(ds,dd,cd,cf):
    global totalBytes, totalBytesCopied, filesSkipped
    dest_path = str(cf).replace(str(ds),'')
    dest_file = dd.append(dest_path)
    dest_path = dest_file.parent()
    sfs = cf.size()
    totalBytes += sfs
    if dest_file.exists():
        dfmt = dest_file.mtime()
        dfs = dest_file.size()
        sfmt = cf.mtime()
        if dfmt == sfmt and dfs == sfs:
            filesSkipped += 1
            echo f'SKIP: {dest_file}'
            return
    # if we made it here, we should be good to copy
    do_copy(cf,dest_path)
    totalBytesCopied += sfs
    echo f'COMPLETE: {dest_file}'

dirs_to_search = [SRC]
dirs_found = 1
files_found = 0
while dirs_to_search:
    cur_dir = dirs_to_search.pop(0)
    echo f'SEARCH: {cur_dir}'
    for entry in cur_dir.iterdir():
        if entry.is_file():
            echo f'FILE: {entry}'
            files_found += 1
            sync_copy(SRC,DEST,cur_dir,entry)
        elif entry.is_dir():
            echo f'DIR: {entry}'
            dirs_to_search.append(entry)
            dirs_found += 1
hrb = sizeof_fmt(totalBytes)
hrcb = sizeof_fmt(totalBytesCopied)
echo f'DONE: found {files_found} files in {dirs_found} dirs.\n      {hrcb} copied out of {hrb} found, {filesSkipped} files skipped'
